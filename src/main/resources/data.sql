INSERT INTO speciality VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 'Information system and technologies', '09.03.02');

INSERT INTO student_group VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 'UIS-211', '3b95b014-61e6-4bdb-9a2c-462033226551');

INSERT INTO student VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 'Eugene', 'Novikov', current_date, 'STUDYING', '3b95b014-61e6-4bdb-9a2c-462033226551');

-- Exams

INSERT INTO exam VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', '1309', current_timestamp, 'true');
INSERT INTO exam_student_group VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', '3b95b014-61e6-4bdb-9a2c-462033226551');

INSERT INTO exam VALUES ('3b95b014-61e6-4bdb-9a2c-462033226550', '1309', current_timestamp, 'true');
INSERT INTO exam_student_group VALUES ('3b95b014-61e6-4bdb-9a2c-462033226550', '3b95b014-61e6-4bdb-9a2c-462033226551');

-- Subjects
INSERT INTO faculty VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 2, 'IUIT');
INSERT INTO cathedra VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 'Automatisation technologies', '3b95b014-61e6-4bdb-9a2c-462033226551');
INSERT INTO subject VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', '3b95b014-61e6-4bdb-9a2c-462033226551', 'Databases');

INSERT INTO professor VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', 'eugene', 'novikov', current_date, 'SENIOR_LECTURER');
INSERT INTO subject_professor VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', '3b95b014-61e6-4bdb-9a2c-462033226551', current_date, current_date);
INSERT INTO speciality_subject VALUES ('3b95b014-61e6-4bdb-9a2c-462033226551', '3b95b014-61e6-4bdb-9a2c-462033226551');