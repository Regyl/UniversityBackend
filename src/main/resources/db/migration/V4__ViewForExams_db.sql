CREATE VIEW exams_per_group AS
SELECT sg.*, e.id as exam_id, e.audience, e.timestamp, e.is_paid
FROM student_group sg
    INNER JOIN exam_student_group esg on sg.id = esg.student_group_id
    INNER JOIN exam e on esg.exam_id = e.id;