CREATE TABLE faculty
(
    ID         UUID PRIMARY KEY,
    CORPUS     INT         NOT NULL UNIQUE,
    NAME       VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE cathedra
(
    ID         UUID PRIMARY KEY,
    NAME       VARCHAR(30) NOT NULL UNIQUE,
    FACULTY_ID UUID        NOT NULL,
    FOREIGN KEY (FACULTY_ID) REFERENCES faculty (ID)
);

CREATE TABLE professor
(
    ID         UUID PRIMARY KEY,
    FIRST_NAME VARCHAR(30) NOT NULL,
    LAST_NAME  VARCHAR(30) NOT NULL,
    BIRTH_DATE DATE        NOT NULL,
    POSITION   VARCHAR(20) NOT NULL
);

CREATE TABLE subject
(
    ID          UUID PRIMARY KEY,
    CATHEDRA_ID UUID        NOT NULL,
    NAME        VARCHAR(30) NOT NULL UNIQUE,
    FOREIGN KEY (CATHEDRA_ID) REFERENCES cathedra (ID)
);

CREATE TABLE subject_professor
(
    subject_id   UUID NOT NULL,
    professor_id UUID NOT NULL,
    start_date   DATE NOT NULL,
    end_date     DATE NOT NULL,
    PRIMARY KEY (subject_id, professor_id, start_date),
    FOREIGN KEY (subject_id) REFERENCES subject (ID),
    FOREIGN KEY (professor_id) REFERENCES professor (ID)
);

CREATE TABLE exam
(
    ID         UUID PRIMARY KEY,
    AUDIENCE   INT       NOT NULL,
    TIMESTAMP  TIMESTAMP NOT NULL,
    IS_PAID    BOOLEAN   NOT NULL,
    UNIQUE (AUDIENCE, TIMESTAMP)
);

CREATE TABLE exam_professor
(
    exam_id      UUID NOT NULL,
    professor_id UUID NOT NULL,
    PRIMARY KEY (exam_id, professor_id),
    FOREIGN KEY (exam_id) REFERENCES exam (ID),
    FOREIGN KEY (professor_id) REFERENCES professor (ID)
);

CREATE TABLE exam_student
(
    exam_id    UUID NOT NULL,
    student_id UUID NOT NULL,
    PRIMARY KEY (exam_id, student_id),
    FOREIGN KEY (exam_id) REFERENCES exam (ID),
    FOREIGN KEY (student_id) REFERENCES student (ID)
);

CREATE TABLE exam_student_group
(
    exam_id          UUID NOT NULL,
    student_group_id UUID NOT NULL,
    PRIMARY KEY (exam_id, student_group_id),
    FOREIGN KEY (exam_id) REFERENCES exam (ID),
    FOREIGN KEY (student_group_id) REFERENCES student_group (ID)
);
