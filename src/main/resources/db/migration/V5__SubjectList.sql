CREATE TABLE speciality_subject
(
    speciality_id UUID NOT NULL,
    subject_id    UUID NOT NULL,
    PRIMARY KEY (speciality_id, subject_id),
    FOREIGN KEY (speciality_id) REFERENCES speciality (ID),
    FOREIGN KEY (subject_id) REFERENCES subject (ID)
);

CREATE FUNCTION professor_subject_list(specialityId UUID, professorId UUID, startDate DATE, endDate DATE)
RETURNS TABLE(id UUID, cathedra_id UUID, name VARCHAR(30))
LANGUAGE plpgsql
    AS $$
    BEGIN
    RETURN QUERY SELECT subj.* FROM subject subj
    INNER JOIN speciality_subject ss ON subj.id = ss.subject_id
    INNER JOIN subject_professor sp ON subj.id = sp.subject_id
    WHERE ss.speciality_id = specialityId AND sp.professor_id=professorId
    AND sp.start_date <= startDate AND sp.end_date >= endDate;

    end; $$
