package com.deepspace.university.api.controller;

import com.deepspace.university.entity.Subject;
import com.deepspace.university.service.SubjectService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/students/groups/subjects")
public record SubjectController(SubjectService service) {

    @GetMapping("/")
    public List<Subject> findAllByProfessorAndSpeciality(@RequestParam UUID specialityId,
                                                         @RequestParam UUID professorId,
                                                         @RequestParam LocalDate startDate,
                                                         @RequestParam LocalDate endDate) {
        return service.findAllByProfessorAndSpeciality(specialityId, professorId, startDate, endDate);
    }
}
