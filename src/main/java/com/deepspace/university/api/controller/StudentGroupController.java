package com.deepspace.university.api.controller;

import com.deepspace.university.entity.StudentGroup;
import com.deepspace.university.repository.StudentGroupRepository;
import com.deepspace.university.service.StudentGroupService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students/groups/")
public record StudentGroupController(StudentGroupService service) {

    @GetMapping("/exams")
    public List<StudentGroup> findAllExams() {
        return service.findAllExams();
    }
}
