package com.deepspace.university.api.controller;

import com.deepspace.university.entity.Student;
import com.deepspace.university.service.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/students")
public record StudentController(StudentService service) {

    @GetMapping("/")
    public List<Student> findAll() {
        return service.findAll();
    }

    @GetMapping("/exams/admitted")
    public List<Student> findAllAdmitted(@RequestParam UUID examId) {
        return service.findAllAdmitted(examId);
    }

    @GetMapping("/exams/paid")
    public List<Student> findAllWithPaidExams() {
        return service.findAllWithPaidExams();
    }
}
