package com.deepspace.university.api.controller.dto.response;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
public abstract class AbstractDtoResponse {

    private final LocalDateTime timestamp;

    AbstractDtoResponse() {
        this.timestamp = LocalDateTime.now();
    }
}
