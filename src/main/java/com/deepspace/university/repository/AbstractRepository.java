package com.deepspace.university.repository;

import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public abstract class AbstractRepository {

    protected final JdbcTemplate jdbcTemplate;

}
