package com.deepspace.university.repository.dto;

import com.deepspace.university.entity.Exam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class StudentGroupExam extends Exam {

    private UUID groupId;

    private String name;
}
