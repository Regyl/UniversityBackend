package com.deepspace.university.repository;

import com.deepspace.university.entity.Subject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public class SubjectRepository extends AbstractRepository {

    private static final RowMapper<Subject> SUBJECT_ROW_MAPPER = new SubjectMapper();

    public SubjectRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    public List<Subject> findAllByProfessorAndSpeciality(UUID specialityId, UUID professorId,
                                                         LocalDate startDate, LocalDate endDate) {
        String query = String.format("SELECT * FROM professor_subject_list('%S', '%S', '%S', '%S')",
                specialityId, professorId, startDate, endDate);
        return jdbcTemplate.query(query, SUBJECT_ROW_MAPPER);
    }

    private static final class SubjectMapper implements RowMapper<Subject> {
        @Override
        public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
            return Subject.builder()
                    .id(rs.getObject("id", UUID.class))
                    .name(rs.getString("name"))
//                    .cathedra() //TODO:
                    .build();
        }
    }
}
