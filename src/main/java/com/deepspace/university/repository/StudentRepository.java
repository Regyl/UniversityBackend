package com.deepspace.university.repository;

import com.deepspace.university.entity.Student;
import com.deepspace.university.entity.StudentGroup;
import com.deepspace.university.entity.enumeration.StudentStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;


@Repository
public class StudentRepository extends AbstractRepository {

    protected static final RowMapper<Student> STUDENT_ROW_MAPPER = new StudentMapper();

    public StudentRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    public List<Student> findAll() {
        String selectAll = "SELECT * FROM student inner join student_group sg on sg.id = student.student_group";
        return jdbcTemplate.query(selectAll, STUDENT_ROW_MAPPER);
    }

    public List<Student> findAllAdmitted(UUID examId) {
        String query = "SELECT es.* FROM exam_student AS es WHERE es.exam_id="+examId.toString();
        return jdbcTemplate.query(query, STUDENT_ROW_MAPPER);
    }

    public List<Student> findAllWithPaidExams() {
        String query = "SELECT s.* FROM student s INNER JOIN exam_student es ON s.id = es.student_id INNER JOIN exam e ON e.id = es.exam_id WHERE e.is_paid=true";
        return jdbcTemplate.query(query, STUDENT_ROW_MAPPER);
    }

    /**
     * Custom mapper
     */
    private static class StudentMapper implements RowMapper<Student> {

        @Override
        public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
            return Student.builder()
                    .id(rs.getObject("id", UUID.class))
                    .firstName(rs.getString("firstName"))
                    .lastName(rs.getString("lastName"))
                    .birthDate(rs.getObject("birthDate", LocalDate.class))
                    .status(rs.getObject("status", StudentStatus.class))
                    .group(rs.getObject("student_group", StudentGroup.class))
                    .build();
        }
    }
}
