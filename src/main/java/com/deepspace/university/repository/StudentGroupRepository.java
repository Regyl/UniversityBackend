package com.deepspace.university.repository;

import com.deepspace.university.entity.Exam;
import com.deepspace.university.entity.StudentGroup;
import com.deepspace.university.repository.dto.StudentGroupExam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public class StudentGroupRepository extends AbstractRepository {

    protected static final RowMapper<StudentGroupExam> STUDENT_GROUP_EXAM_MAPPER = new StudentGroupExamMapper();

    public StudentGroupRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    public List<StudentGroupExam> findAllExams() {
        String query = "SELECT * FROM exams_per_group"; //View, defined in V4 migration
        return jdbcTemplate.query(query, STUDENT_GROUP_EXAM_MAPPER);
    }

    private static final class StudentGroupExamMapper implements RowMapper<StudentGroupExam> {
        @Override
        public StudentGroupExam mapRow(ResultSet rs, int rowNum) throws SQLException {
            return StudentGroupExam.builder()
                    .id(rs.getObject("exam_id", UUID.class))
                    .audience(rs.getInt("audience"))
                    .timestamp(rs.getObject("timestamp", LocalDateTime.class))
                    .groupId(rs.getObject("id", UUID.class))
                    .name(rs.getString("name"))
                    .build();
        }
    }

}
