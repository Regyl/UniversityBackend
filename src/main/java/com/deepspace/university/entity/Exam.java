package com.deepspace.university.entity;

import com.deepspace.university.repository.dto.StudentGroupExam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Exam extends AbstractEntity {

    private Integer audience; //аудитория

    private LocalDateTime timestamp;

    public static Exam of(StudentGroupExam exam) {
        return Exam.builder()
                .id(exam.getId())
                .audience(exam.getAudience())
                .timestamp(exam.getTimestamp())
                .build();
    }

}
