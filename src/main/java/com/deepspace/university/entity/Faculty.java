package com.deepspace.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Faculty extends AbstractEntity {

    private String name;

    private Integer corpus;
    
    private List<Cathedra> cathedraList;
}
