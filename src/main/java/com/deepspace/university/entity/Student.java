package com.deepspace.university.entity;

import com.deepspace.university.entity.enumeration.StudentStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Student extends AbstractHumanEntity {

    private StudentGroup group;

    private StudentStatus status;

}
