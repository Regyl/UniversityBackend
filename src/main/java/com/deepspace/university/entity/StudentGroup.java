package com.deepspace.university.entity;

import com.deepspace.university.repository.dto.StudentGroupExam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class StudentGroup extends AbstractEntity {

    private String name;

    private List<Exam> exams;

    public static StudentGroup of(StudentGroupExam exam) {
        return StudentGroup.builder()
                .id(exam.getGroupId())
                .name(exam.getName())
                .build();
    }
}
