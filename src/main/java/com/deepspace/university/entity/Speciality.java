package com.deepspace.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Speciality extends AbstractEntity {

    private String number;

    private String name;

}
