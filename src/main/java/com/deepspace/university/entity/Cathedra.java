package com.deepspace.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Cathedra extends AbstractEntity {

    private Faculty faculty;

    private String name;

    private List<Professor> professors;
}
