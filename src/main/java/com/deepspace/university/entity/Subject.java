package com.deepspace.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Subject extends AbstractEntity {

    private String name;

    private Cathedra cathedra;
}
