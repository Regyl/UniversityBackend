package com.deepspace.university.entity.enumeration;

public enum Position {
    SENIOR_LECTURER,
    DOCENT,
    PROFESSOR,
    ASSISTANT_PROFESSOR
}
