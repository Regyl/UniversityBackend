package com.deepspace.university.entity.enumeration;

public enum StudentStatus {
    EXPELLED, //Отчислен
    STUDYING, //Обучается
    SABBATICAL //В академе
}
