package com.deepspace.university.entity;

import com.deepspace.university.entity.enumeration.Position;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class Professor extends AbstractHumanEntity {

    private Position position;

    private List<Subject> subjects;

    private Cathedra cathedra;

    private List<Exam> exams;

}
