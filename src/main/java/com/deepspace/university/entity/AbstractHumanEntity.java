package com.deepspace.university.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractHumanEntity extends AbstractEntity {

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

}
