package com.deepspace.university.service;

import com.deepspace.university.entity.Subject;
import com.deepspace.university.repository.SubjectRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public record SubjectService(SubjectRepository repository) {

    public List<Subject> findAllByProfessorAndSpeciality(UUID specialityId, UUID professorId,
                                                         LocalDate startDate, LocalDate endDate) {
        return repository.findAllByProfessorAndSpeciality(specialityId, professorId, startDate, endDate);
    }
}
