package com.deepspace.university.service;

import com.deepspace.university.entity.Exam;
import com.deepspace.university.entity.StudentGroup;
import com.deepspace.university.repository.StudentGroupRepository;
import com.deepspace.university.repository.dto.StudentGroupExam;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

@Service
public record StudentGroupService(StudentGroupRepository repository) {

    public List<StudentGroup> findAllExams() {
        List<StudentGroupExam> examsPerGroup = repository.findAllExams();
        List<StudentGroup> groups = examsPerGroup.stream()
                .filter(distinctByKey(StudentGroupExam::getGroupId))
                .map(StudentGroup::of)
                .toList();
        groups.forEach(group -> {
            List<Exam> exams = examsPerGroup.stream()
                    .filter(item -> item.getGroupId().equals(group.getId()))
                    .map(Exam::of)
                    .toList();
            group.setExams(exams);
        });
        return groups;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
