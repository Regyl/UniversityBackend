package com.deepspace.university.service;

import com.deepspace.university.entity.Student;
import com.deepspace.university.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public record StudentService(StudentRepository repository) {

    public List<Student> findAll() {
        return repository.findAll();
    }

    public List<Student> findAllAdmitted(UUID examId) {
        return repository.findAllAdmitted(examId);
    }

    public List<Student> findAllWithPaidExams() {
        return repository.findAllWithPaidExams();
    }
}
